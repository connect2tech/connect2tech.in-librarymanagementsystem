package com.c2t.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ReadObject {
	static Properties p = new Properties();

	public static Properties getObjectRepository() {
		
		try {
			// Read object repository file
			InputStream stream = new FileInputStream(
					new File("lms.properties"));
			// load all objects
			p.load(stream);
		
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return p;
	}
	
	
	public static void main(String[] args) {
		ReadObject obj = new ReadObject();
		try {
			Properties prop = obj.getObjectRepository();
			System.out.println(prop);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		}
	}
}
